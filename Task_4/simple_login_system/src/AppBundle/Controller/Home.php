<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class Home
{
    /**
     * @Route("/home")
     */
    public function index()
    {
        return new Response(
            '<html><body>Home</body></html>'
        );
    }
}
