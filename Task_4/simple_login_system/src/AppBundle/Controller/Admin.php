<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class Admin
{
    /**
     * @Route("/admin")
     */
    public function index()
    {
        return new Response(
            '<html><body>Admin</body></html>'
        );
    }
}
